#include "Gnuplot.hh"

void Gnuplot::clear() 
{ 
    *this < " clear\n";
}

void Gnuplot::set(const char* option) 
{
    *this << " set " << option < "\n"; 
}

void Gnuplot::plot(double* x, int x_N, int x_step, double* y)
{
    *this << " plot  '-' using 1:2\n";

    //For to plot smooth functions consisting of many datapoints efficiently, set x_step>1
    int i;
    for( i = 0; i < x_N; i += x_step ) {
        *this << x[i] << " " << y[i] << "\n";
    }

    *this < "e\n";
}

void Gnuplot::splot(double* x, int x_N, int x_step, double* y, int y_N, int y_step, double** z)
{
    *this << " splot  '-' using 1:2:3\n";

    //For to plot smooth functions consisting of many datapoints efficiently, set x_step>1, y_step>1
    int i;
    int j;
    for( i = 0; i < x_N; i += x_step )
    {
        for( j = 0; j < y_N; j += y_step )
        {
            //Note the order in which the loops are nested
            *this << x[i] << " " << y[j] << " " << z[i][j] << "\n";
        }
        //Certain gnuplot functionality requires an empty line when the x value changes
        *this << "\n";
    }
    
    *this < "e\n";
}
