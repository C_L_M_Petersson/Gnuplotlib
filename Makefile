#------------------------------------------------------------------------------

SOURCE=GnuplotBase.hh Gnuplot.hh Gnuplot.cc
PROGRAM=gnuplotlib.so
INCLUDE=
LIBRARIES=
FLAGS=-fPIC -shared -Wall
CC=g++

#------------------------------------------------------------------------------

all: $(PROGRAM)

$(PROGRAM): $(SOURCE)
	$(CC) $(SOURCE) -o$(PROGRAM) $(INCLUDE) $(LIBRARIES) $(FLAGS)

#------

.PHONY: force
force: 
	$(CC) $(SOURCE) -o $(PROGRAM) $(INCLUDE) $(LIBRARIES) $(FLAGS)

#------

.PHONY: optimised
optimised:
	$(CC) $(SOURCE) -o $(PROGRAM) $(INCLUDE) $(LIBRARIES) $(FLAGS) -O2

#------

.PHONY: clean
clean:
	rm -f $(PROGRAM)
