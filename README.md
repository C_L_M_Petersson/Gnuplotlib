# README #

### Summary ###
 * The files Gnuplot.hh and Gnuplot.cc defines a class, Gnuplot, which interfaces with an instance of gnuplot.
 * Gnuplot is derived from classes defined in GnuplotBase.hh.
 * Only a few predefined commands can be used to interface with gnuplot. This ensures readability of both gnuplot.hpp and the code which creates the instance of Gnuplot.
 * More commands can be defined in Gnuplot.hh if needed.
 * The Makefile contains the options all, force, optimised, and clean.
 * Note that this code only has been tested on linux. I don't know how well it would function on other operating systems.

### Usage ###
 * Use the makefile to create a .so library (preferably via $make optimised), which can be linked to externally, **OR** compile gnuplot.hpp together with the rest of the C++ code, by including it as any other header file.
 * Create an instance of the Gnuplot class. gnuplot can now be used by calling the public functions of the Gnuplot instance.
 * The functions implemented so far are the gnuplot functions clear, set [option], plot, and splot. Note that both plot and splot are implemented to work with vectors, not files.

### Adding functionallity ###
 * The << operator has been overloaded, and can be used to pipe doubles and char arrays to gnuplot.
 * The < operator has also been overloaded. It pipes a char array to gnuplot and then flushes the buffer. Use it for the last pipe of each function.
 * See Gnuplot.hh for examples.

### Issues with the current version/TODO ###

 * Sometimes (randomly?) when commands are piped to gnuplot, the first character of the command will disappear, resulting in faulty commands to gnuplot. As a workaround to this, every function starts by adding a space to the buffert. This means that it no longer matters if first character of the command disappears.
 * The code (presumably on the gnuplot end) sometimes run slowly. This only happens after a large amount of points already have been plotted.
 * Change from popopen() to execve() or execle().
 * More functionallity will be added as time goes on.
 * Add gnuplot options in the constructor, such as -persist.